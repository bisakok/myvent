<?php
SESSION_START();
include 'koneksi.php';
if(!isset($_SESSION['userid'])) {
  echo "<script>setTimeout(\"location.href='login.php';\",0);</script>";
}
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include 'core/header.php';?>
    <style media="screen">
    .value-button {
      display: inline-block;
      border: 1px solid #ddd;
      margin: 0px;
      width: 40px;
      height: 40px;
      text-align: center;
      vertical-align: middle;
      /* padding: px 0; */
      background: #eee;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .value-button:hover {
      cursor: pointer;
    }

    form #decrease {
      margin-top: -2px;
      margin-right: -4px;
      border-radius: 8px 0 0 8px;
      font-size: 25px;
      }

    form #increase {
      font-size: 25px;
      margin-top: -2px;
      margin-left: -4px;
      border-radius: 0 8px 8px 0;
    }

    form #input-wrap {
      margin: 0px;
      padding: 0px;
    }

    input#number {
      text-align: center;
      border: none;
      border-top: 1px solid #ddd;
      border-bottom: 1px solid #ddd;
      margin: 0px;
      width: 90px;
      height: 40px;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    </style>
  </head>
  <body>
    <?php include 'core/menu.php';?>
      <div class="row" style="margin:0px 20px 10px 20px">
        <?php
        $ambil_data =  mysqli_query($koneksi,"SELECT * FROM `events` WHERE `id_event` = '$_GET[id]'");
        while ($a = mysqli_fetch_assoc($ambil_data)):
         ?>

      <center>  <h2><?php echo $a['title'] ?></h2></center>
        <div class="col-xs-12">
          <img src="<?php echo $a['pic'] ?>" class="img img-responsive">
        </div>
        <div class="col-xs-12">
<br>
<center>
          <form action="" method="post">
            <div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
             <input type="number" name="jumlah" id="number" value="1"/>
             <div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">+</div>
             <br><br>

             <input type="submit" class="btn btn-block" name="order" value="BUY" style="background-color:#0d2a4a;color:#FFF;padding:10px 20px 10px 20px;">
             <br>
          </div>
          </form>

</center>

<h4><b>INFO</b></h4>
<p>Rp. <?php echo number_format($a['harga'],0,',','.') ?></p>
<p><?php echo $a['location'] ?></p>
<p><?php echo $a['tanggal'] ?></p>
<p><?php echo $a['waktu'] ?> WIB</p>
<p><?php echo $a['deskripsi'] ?></p>

<hr>
<?php
if(isset($_POST['order'])){
$jumlah = $_POST['jumlah'];
$tiketID = $a['id_event'];
$tanggal = date("Y-m-d");
$lihat_data = mysqli_query($koneksi,"SELECT * FROM `pesan` WHERE `id_account`='$_SESSION[userid]' AND `id_events`=$tiketID");
$jdb = mysqli_fetch_assoc($lihat_data);
$ada = mysqli_fetch_assoc($lihat_data);
if ($ada > 0) {
  $jumlah = $jumlah + $jdb['jumlah'];
  $update = mysqli_query($koneksi,"UPDATE `pesan` SET
    `jumlah`= $jumlah WHERE `id_account`='$_SESSION[userid]' AND `id_events`='$tiketID'");
  if($update){
    $_SESSION['id_event'] = $tiketID;
    $_SESSION['jumlah'] = $jumlah;
    echo "<script>setTimeout(\"location.href='buy.php';\",0);</script>";
  }else {
    echo "<script>setTimeout(\"location.href='order.php?id=$_GET[id]';\",2000);</script>";
  }

}else {
  $insert = mysqli_query($koneksi,"INSERT INTO `pesan`(`id_pesanan`, `order_time`, `oder_buy`,
     `id_account`, `id_events`, `jumlah`, `isOrder`, `isBuy`)
  VALUES ('','$tanggal','','$_SESSION[userid]',$tiketID,$jumlah,1,0)");
  if($insert){
    $_SESSION['id_event'] = $tiketID;
    $_SESSION['jumlah'] = $jumlah;
    echo "<script>setTimeout(\"location.href='buy.php';\",0);</script>";
  }else {
    echo "<script>setTimeout(\"location.href='order.php?id=$_GET[id]';\",2000);</script>";
  }
}
}

 ?>
<?php endwhile; ?>
</div>

      </div>
      <center><h3>Rekomendasi Event</h3></center>

      <div class="col-xs-12" style="margin-bottom:80px">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="event/1.jpg">
            </div>
          </div>
        </div>
      </div>

      <br><br><br>

    <div style="margin-bottom:80px"></div>
      <?php include 'core/menu_bawah.php';?>

    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    function increaseValue() {
var value = parseInt(document.getElementById('number').value, 10);
value = isNaN(value) ? 0 : value;
value++;
document.getElementById('number').value = value;
}

function decreaseValue() {
var value = parseInt(document.getElementById('number').value, 10);
value = isNaN(value) ? 0 : value;
value < 1 ? value = 1 : '';
value--;
document.getElementById('number').value = value;
}
    </script>
  </body>
</html>
