<?php
SESSION_START();
include 'koneksi.php';
if(!isset($_SESSION['userid'])) {
  echo "<script>setTimeout(\"location.href='login.php';\",0);</script>";
}
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
  <?php
  include 'core/header.php';
   ?>
    <style media="screen">

    .value-button {
      display: inline-block;
      border: 1px solid #ddd;
      margin: 0px;
      width: 40px;
      height: 40px;
      text-align: center;
      vertical-align: middle;
      /* padding: px 0; */
      background: #eee;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .value-button:hover {
      cursor: pointer;
    }

    form #decrease {
      margin-right: -4px;
      border-radius: 8px 0 0 8px;
    }

    form #increase {
      margin-left: -4px;
      border-radius: 0 8px 8px 0;
    }

    form #input-wrap {
      margin: 0px;
      padding: 0px;
    }

    input#number {
      text-align: center;
      border: none;
      border-top: 1px solid #ddd;
      border-bottom: 1px solid #ddd;
      margin: 0px;
      width: 40px;
      height: 40px;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    </style>
  </head>
  <body>

    <?php
    include 'core/menu.php';
     ?>

          <div class="info" style="font-size:20px;  margin-top:10px">
            <?php
                $data = mysqli_query($koneksi,"SELECT * FROM `pesan` left JOIN events ON pesan.id_events=events.id_event RIGHT JOIN account ON pesan.id_account=account.id_account WHERE pesan.id_account='$_SESSION[userid]' AND `id_events`='$_SESSION[id_event]'");
                $a = mysqli_fetch_assoc($data);
                // var_dump($a);
            $totalprize = $_SESSION['jumlah'] * $a['harga'];
             ?>
             <h3><ul>
               <li><?php echo $a['title'] ?></li>
             </ul></h3>

             <div class="col-xs-6">
               <img src="<?php echo $a['pic'] ?>" alt="" class="img img-responsive">
             </div>

             <div class="col-xs-6">
               <p>OPEN GATE : <?php echo $a['waktu'] ?></p>
               <p>TICKET PRIZE : Rp. <?php echo number_format($a['harga'],0,",",".")  ?> </p>
               <p>TOTAL TIKET : <?php echo $_SESSION['jumlah'] ?></p>
               <p>TOTAL PRIZE: <?php echo number_format($totalprize,0,",",".")  ?></p>
               <img src="pay.png" alt="" class="img img-responsive">
               <form action="" method="post">
                 <input type="submit" class="btn btn-block" name="buy" value="PAY" style="background-color:#0d2a4a;color:#FFF;padding:10px 20px 10px 20px;">
               </form>


             </div>

             </div>

          <?php
          if(isset($_POST['buy'])){
            $update = mysqli_query($koneksi,"UPDATE `pesan` SET `isOrder`=0,`isBuy`=1 WHERE `id_events`='$a[id_event]' AND `id_account`='$_SESSION[userid]'" );
            // $update = mysqli_query($koneksi,"")
            $tujuan = '628993769099';
            $jumlah = $a['jumlah']*$a['harga'];
            $text = "Hai ada pesanan baru,\n nama : $a[nama] \n email : $a[email] \n DETAIL TIKET \n \n judul acara : $a[title]\n harga satuan : Rp. $a[harga] \n jumlah dibeli : Rp. $a[jumlah] \n TOTAL :$jumlah ";
            $kirim = urlencode($text);
            $kirim2 = "https://api.whatsapp.com/send?phone=$tujuan&text=$kirim";
            echo "<script>setTimeout(\"location.href='$kirim2';\",2000);</script>";
          }
           ?>

           <?php
           include 'core/menu_bawah.php';
            ?>
    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
