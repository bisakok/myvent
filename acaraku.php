<?php
SESSION_START();
include 'koneksi.php';
if(!isset($_SESSION['userid'])) {
  echo "<script>setTimeout(\"location.href='login.php';\",0);</script>";
}
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include 'core/header.php';?>
  </head>
  <body>
    <?php include 'core/menu.php';?>

      <div class="row" style="margin:0px 20px 10px 20px">
        <?php
        $ambil_data = mysqli_query($koneksi,"SELECT * FROM `events` WHERE `id_user` = '$_SESSION[userid]'");
        while ($a = mysqli_fetch_assoc($ambil_data)):
         ?>
         <center>  <h2><?php echo $a['title'] ?></h2></center>
           <div class="col-xs-12">
             <img src="<?php echo $a['pic'] ?>" class="img img-responsive">
           </div>
           <div class="col-xs-12">
   <br>
         </div>
         <h4><b>INFO</b></h4>
         <p>Rp. <?php echo number_format($a['harga'],0,',','.') ?></p>
         <p><?php echo $a['location'] ?></p>
         <p><?php echo $a['tanggal'] ?></p>
         <p><?php echo $a['waktu'] ?> WIB</p>
         <p><?php echo $a['deskripsi'] ?></p>
         <hr>

       <?php endwhile; ?>

    </div>
    <div style="margin-bottom:80px"></div>
      <?php include 'core/menu_bawah.php';?>

    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
