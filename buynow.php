<?php
SESSION_START();
include 'koneksi.php';
if(!isset($_SESSION['userid'])) {
  echo "<script>setTimeout(\"location.href='login.php';\",0);</script>";
}
$userId  = $_GET['userid'];
$tiketId = $_GET['tiket_id'];
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include 'core/header.php';?>
    <style media="screen">
    .value-button {
      display: inline-block;
      border: 1px solid #ddd;
      margin: 0px;
      width: 40px;
      height: 40px;
      text-align: center;
      vertical-align: middle;
      /* padding: px 0; */
      background: #eee;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .value-button:hover {
      cursor: pointer;
    }

    form #decrease {
      margin-right: -4px;
      border-radius: 8px 0 0 8px;
    }

    form #increase {
      margin-left: -4px;
      border-radius: 0 8px 8px 0;
    }

    form #input-wrap {
      margin: 0px;
      padding: 0px;
    }

    input#number {
      text-align: center;
      border: none;
      border-top: 1px solid #ddd;
      border-bottom: 1px solid #ddd;
      margin: 0px;
      width: 40px;
      height: 40px;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    </style>
  </head>
  <body>
    <?php include 'core/menu.php';?>


      <?php
      // $data = mysqli_query($koneksi,"SELECT * FROM `oder_tiket`
        $data = mysqli_query($koneksi,"SELECT * FROM `oder_tiket`
          LEFT JOIN tiket ON oder_tiket.tiketID = tiket.tiket_id
          WHERE `userID` = '$userId' && oder_tiket.tiketID='$tiketId'");
        $a = mysqli_fetch_assoc($data);
       ?>
          <div class="info" style="font-size:20px;  margin-top:10px">
            <?php
            $totalprize = $a['jumlah'] * $a['harga'];
             ?>
            <p style="font-weight:bold">APAKAH ADA YAKIN UNTUK MEMBAYAR TIKET DENGAN DETAIL BERIKUT INI?</p>
            <p>JUDUL ACARA : <?php echo $a['judul'] ?></p>
            <p>OPEN GATE : <?php echo $a['jam'] ?></p>
            <p>TICKET PRIZE : <?php echo "Rp ".number_format($a['harga'],0,",",".") ?></p>
            <p>TOTAL TIKET : <?php echo $a['jumlah']; ?></p>
            <p>TOTAL PRIZE: <?php echo "Rp ". number_format($totalprize,0,",",".")  ?></p>
            <p></p>
          </div>

          <form action="" method="post">
            <input type="submit" class="btn btn-block" name="buy" value="YES" style="background-color:#0d2a4a;color:#FFF;padding:10px 20px 10px 20px;">
            <input type="submit" class="btn btn-block" name="buy" value="NO" style="background-color:#255489;color:#FFF;padding:10px 20px 10px 20px;">

          </form>
          <?php
          if(isset($_POST['buy'])){
            $pilihan = $_POST['buy'];
            if($pilihan == "YES"){

              $insert = mysqli_query($koneksi, "INSERT INTO `buy`(`id`, `userID`, `tiketID`, `jumlah`)
              VALUES ('','$a[userID]','$a[tiketID]','$a[jumlah]')");
              if($insert){
                // echo "berhasil memindah";
                $hapus = mysqli_query($koneksi,"DELETE FROM `oder_tiket` WHERE `userID`='$userId' && `tiketID`='$tiketId'");
                if($hapus){
                    echo "<script>setTimeout(\"location.href='terbeli.php';\",2000);</script>";
                }else {
                  $hapus = mysqli_query($koneksi,"DELETE FROM `oder_tiket` WHERE `userID`='$userId' && `tiketID`='$tiketId'");
                }
              }else {
                echo "<script>setTimeout(\"location.href='masih_order.php';\",2000);</script>";
              }
            }elseif($pilihan == "NO") {
              echo "<script>setTimeout(\"location.href='masih_order.php';\",2000);</script>";
            }
          }
           ?>
           <?php include 'core/menu_bawah.php';?>

    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
