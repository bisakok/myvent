<?php
SESSION_START();
include 'koneksi.php';
if(!isset($_SESSION['userid'])) {
  echo "<script>setTimeout(\"location.href='login.php';\",0);</script>";
}
 ?>
<!DOCTYPE html>
<html lang="en">
    <head>
      <?php include 'core/header.php'; ?>
      <style media="screen">
      .value-button {
        display: inline-block;
        border: 1px solid #ddd;
        margin: 0px;
        width: 40px;
        height: 40px;
        text-align: center;
        vertical-align: middle;
        /* padding: px 0; */
        background: #eee;
        -webkit-touch-callout: none;
        -webkit-user-select: none;
        -khtml-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      .value-button:hover {
        cursor: pointer;
      }

      form #decrease {
        margin-top: -4px;
        margin-right: -4px;
        border-radius: 8px 0 0 8px;
        font-size: 25px;
        }

      form #increase {
        font-size: 25px;
        margin-top: -4px;
        margin-left: -4px;
        border-radius: 0 8px 8px 0;
      }

      form #input-wrap {
        margin: 0px;
        padding: 0px;
      }

      input#number {
        text-align: center;
        border: none;
        border-top: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
        margin: 0px;
        width: 50px;
        height: 40px;
      }

      input[type=number]::-webkit-inner-spin-button,
      input[type=number]::-webkit-outer-spin-button {
          -webkit-appearance: none;
          margin: 0;
      }
      </style>
    </head>
  <body>
    <?php
    include 'core/menu.php';
    $data = mysqli_query($koneksi,"SELECT * FROM `pesan` LEFT JOIN events ON pesan.id_events=events.id_event LEFT JOIN account ON pesan.id_account=account.id_account WHERE pesan.id_account = '$_SESSION[userid]' && pesan.jumlah!=0 && pesan.isOrder=1");
    $ada = mysqli_num_rows($data);?>

      <?php
      if($ada > 0):
        while ($a = mysqli_fetch_assoc($data)):
        ?>
        <h3><ul>
          <li><?php echo $a['title'] ?></li>
        </ul></h3>

          <!-- <div class="row"> -->
            <div class="col-xs-6">
              <img src="<?php echo $a['pic'] ?>" alt="" class="img img-responsive">
            </div>
            <div class="col-xs-6">
              <br><br><br>
              <center>
                        <form action="" method="post">
                          <div class="value-button" id="decrease" onclick="decreaseValue()" value="Decrease Value">-</div>
                           <input type="number" name="jumlah" id="number" value="<?php echo $a['jumlah'] ?>"/>
                           <div class="value-button" id="increase" onclick="increaseValue()" value="Increase Value">+</div>
                           <br><br>
                           <input type="submit" class="btn btn-block" name="buy" value="PAY" style="background-color:#0d2a4a;color:#FFF;padding:10px 20px 10px 20px;">
                           <br>
                        </form>

              </center>
            </div>

            <?php
            if(isset($_POST['buy'])){

              $update = mysqli_query($koneksi,"UPDATE `pesan` SET `isOrder`=0,`isBuy`=1 WHERE `id_events`='$a[id_event]' AND `id_account`='$_SESSION[userid]'" );
              $tujuan = '628993769099';
              $jumlah = $a['jumlah']*$a['harga'];
              $text = "Hai ada pesanan baru,\n nama : $a[nama] \n email : $a[email] \n \n DETAIL TIKET \n
              judul acara : $a[title]\n harga satuan : $a[harga] \n jumlah dibeli : $a[jumlah] \n
              TOTAL :$jumlah ";
              $kirim = urlencode($text);
              $kirim2 = "https://api.whatsapp.com/send?phone=$tujuan&text=$kirim";
              echo "<script>setTimeout(\"location.href='$kirim2';\",2000);</script>";


            }
             ?>


          <!-- </div> -->
      <?php endwhile;?>
    <?php else:?>
        <div class="tengah"  style="margin-top:0%">
          <center><img src="logo.png" class="img" style="width:50%;">
          <br><br>
          <h5 style="font-size:17pt;font-weight:bold">Hmmm... Masih Kosong</h5></center>
        </div>
      <?php endif; ?>



    <?php
    include 'core/menu_bawah.php';
     ?>
     <script type="text/javascript">
     function increaseValue() {
 var value = parseInt(document.getElementById('number').value, 10);
 value = isNaN(value) ? 0 : value;
 value++;
 document.getElementById('number').value = value;
 }

 function decreaseValue() {
 var value = parseInt(document.getElementById('number').value, 10);
 value = isNaN(value) ? 0 : value;
 value < 1 ? value = 1 : '';
 value--;
 document.getElementById('number').value = value;
 }
     </script>
    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
