<?php
SESSION_START();
include 'koneksi.php';
if(isset($_SESSION['userid'])) {
  echo "<script>setTimeout(\"location.href='dashboard.php';\",1000);</script>";
}
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include 'core/header.php'; ?>
  </head>
  <body>
    <div class="tengah"  style="margin-top:30%">
      <center><img src="logo.png" class="img" style="width:50%;">
      <br><br>
      <h5 style="font-size:17pt;font-weight:bold">Join myVent Today</h5></center>
      <a class="btn btn-warning btn-info btn-lg btn-block" href="login.php" role="button" style="background-color:#0d2a4a !important">LOGIN</a>
      <br>
      <a class="btn btn-warning btn-info btn-lg btn-block" href="signup.php" role="button" style="background-color:#0d2a4a !important">SIGN UP</a>

    </div>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
