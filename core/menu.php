<?php
include 'koneksi.php';
$ambil_data = mysqli_query($koneksi,"SELECT * FROM `genre`");
$ada = mysqli_num_rows($ambil_data);
 ?>
<style media="screen">
  #anchor{
    margin-left: 20px;
    color:white;
    display:block;
    font-size:14px;
    margin-bottom:10px;
  }
</style>
<div id="custom-bootstrap-menu" class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboard.php">
              <center><img alt="Brand" src="logo.png" class="logo_atas"></center>
          </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-menubuilder"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse navbar-menubuilder">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="dashboard.php">Home</a>
                </li>
                <li><a href="account.php">Account</a>
                </li>

                        <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Event
                        </a>
                          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <?php if ($ada > 0): ?>
                              <?php while ($a = mysqli_fetch_assoc($ambil_data)): ?>
                                <a id="anchor" class="dropdown-item" href="event.php?genre=<?php echo $a['id_genre'] ?>"><?php echo $a['name'] ?></a>
                            <?php endwhile; ?>
                            <?php endif; ?>
                      </div>
                      </li>
                <li><a href="kontak.php">Contact Us</a>
                </li>
                <li><a href="info.php">Info MyVent</a>
                </li>
                <li><a href="keluar.php">keluar</a>
                </li>

            </ul>
        </div>
    </div>
</div>
<br><br><br>
