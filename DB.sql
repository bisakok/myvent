-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 12, 2019 at 08:14 AM
-- Server version: 10.2.29-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u8980109_event`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id_account` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `pp` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id_account`, `nama`, `email`, `pass`, `pp`) VALUES
(1, 'ahmad busro nuddin', 'djbuzro@gmail.com', '45f61719c2318100be968858dae1820f', ''),
(2, 'Naufal', 'naufal.hilmy093@gmail.com', '202cb962ac59075b964b07152d234b70', '');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id_event` int(11) NOT NULL,
  `id_genre` int(3) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `tanggal` varchar(20) DEFAULT NULL,
  `waktu` varchar(10) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(2000) DEFAULT NULL,
  `id_user` int(3) DEFAULT NULL,
  `harga` int(8) DEFAULT NULL,
  `jumlah_tiket` int(4) DEFAULT NULL,
  `sisa_tiket` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id_event`, `id_genre`, `title`, `tanggal`, `waktu`, `pic`, `location`, `deskripsi`, `id_user`, `harga`, `jumlah_tiket`, `sisa_tiket`) VALUES
(1, 1, 'BACK TO THE 90s', '2019-08-20', '18:00:00', 'img/poster/1.jpg', 'Halaman Unisma Malang', 'UPERFRIENDS MALANG PRESENTS:\r\n\r\nBACK TO THE 90s\r\n\r\nJumat, December 13th 2019\r\n3 PM - End\r\nHalaman Unisma Malang\r\nFree Admission\r\n\r\nBersenang-senang sembari bernostalgia mengenang indahnya era tahun 90an dalam sebuah event penuh canda ria.\r\n\r\nAdalah Kolaborasi komunitas untuk mengulang kembali atmosfir era 90an dengan beragam konten menarik (Community Booth, Games, Tenant, Marketplace, Workshop, Music Festival).\r\n-\r\nMenampilkan:\r\n• Javaganza @javaganzaband\r\n• Audiosick @audiosickofficial\r\n• Flop All Star @malangflop\r\n• Faxon @faxonband_official\r\n• Theatering @theateringofficial\r\n• Gypsum Kid\r\n• WARKOP DKI Iconic Trio (by Orleysen, Dewangga, Soba)\r\n\r\nFeel the era, come around and be the era!\r\n\r\nSUPPORTED BY:\r\n• LAZONE.ID @lazone.id\r\n• UKM GAUNG @musikgaung193\r\n• NGALAM JAZZ COMMUNITY @njc_ngalam\r\n• MALANG FLOP @malangflop\r\n• MALANG SUPERMOSHPIT @malangsupermoshpit\r\n• PORTAL MUSIK MALANG @portalmusikmalang\r\n• JEKS 77 PRODUCTION @jeks77musik\r\n• HARAJUKU MUSIC @harajukumusik\r\n\r\nINFORMASI:\r\n@backtothe90mlg @superfriendsmalang', 1, 100000, 1000, 1000),
(2, 1, 'LAGONA 2.0', '0000-00-00', '17:00:00', 'img/poster/2.jpg', 'Auditorium Kampus B UNUSA (Theater Seat)', '[BEM FK UNUSA] proudly present:\r\nðŸŽ‰LAGONA 2.0ðŸŽ‰\r\nWhoâ€™s excited for lagona 2.0 ?ðŸ™ŒðŸ»\r\nMalam Puncak Milad Fakultas Kedokteran dengan tema â€œNight Of Diversityâ€ dan tahun ini kita terbuka untuk umum so be ready guys!\r\nSpecial Performance of our Guest Star:\r\nðŸŽ¤LETTOâœ¨\r\nand many more!\r\n\r\nSave the date! Saturday, 14 December 2019\r\nðŸ¤: Auditorium Kampus B UNUSA (Theater Seat)\r\nOpen Gate: 17.00 WIBâ—ï¸ PRE SALE 1: 60K\r\nPRE SALE 2: 65K\r\n+Tiket Seminar Kewirausahaan\r\nPRE SALE 3: 70K (WITH SPESIAL PRICE!!!!!)\r\nOTS: 75K\r\n\r\nFor more information, please kindly contact\r\nðŸ“ž: 081259891717 (Iga Sukmawati)\r\nLine: zalfaoey (Zalfa Putri LD)\r\n\r\nItâ€™s going to be a great event so donâ€™t miss it. See you thereâ€¼ï¸ðŸŽŠ', 1, 60000, 300, 300),
(3, 1, 'Collabonation on Stage Surabaya', '2019/12/14', '20:00', 'img/poster/3.jpg', ' Plaza Surabaya', 'Collabonation on Stage Surabaya\r\n14 Desember 2019 @ Plaza Surabaya\r\n\r\nBersama @kuntoajiw @penerbangroket @asteriska_ @soundwaveofficial_ @float_project\r\nSee you mates!\r\n\r\n#Collabonation\r\n#IM3OoredooXschool\r\n#FreedomInternet\r\n#floatchoir\r\n#float\r\n#floatmates\r\n#kuntoaji\r\n#penerbangroket\r\n#asteriska\r\n#soundwave\r\n#eventmusiksurabaya\r\n#funwithfloofi', 1, 20000, 100, 100),
(4, 1, 'Suara pengembara Nada', '2019/12/14', '19:00', 'img/poster/3.jpg', 'Unesa Lidah Wetan', 'No Description', 1, 50000, 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

CREATE TABLE `genre` (
  `id_genre` int(11) NOT NULL,
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id_genre`, `name`) VALUES
(1, 'Musik'),
(2, 'Automotive'),
(3, 'Book Sale'),
(4, 'Seminar'),
(5, 'Festival'),
(6, 'Competition');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesanan` int(11) NOT NULL,
  `order_time` varchar(60) DEFAULT NULL,
  `oder_buy` varchar(60) DEFAULT NULL,
  `id_account` int(3) DEFAULT NULL,
  `id_events` int(3) DEFAULT NULL,
  `jumlah` int(4) DEFAULT NULL,
  `isOrder` int(1) DEFAULT NULL,
  `isBuy` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesanan`, `order_time`, `oder_buy`, `id_account`, `id_events`, `jumlah`, `isOrder`, `isBuy`) VALUES
(5, '2019-12-08', '', 1, 1, 8, 0, 1),
(8, '2019-12-08', '', 1, 1, 8, 0, 1),
(9, '2019-12-11', '', 2, 1, 25, 0, 1),
(10, '2019-12-11', '', 2, 1, 25, 0, 1),
(11, '2019-12-11', '', 1, 4, 1, 0, 1),
(12, '2019-12-12', '', 2, 2, 2, 0, 1),
(13, '2019-12-12', '', 2, 2, 2, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id_account`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id_event`),
  ADD KEY `id_genre` (`id_genre`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id_genre`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesanan`),
  ADD KEY `id_account` (`id_account`),
  ADD KEY `id_events` (`id_events`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `genre`
--
ALTER TABLE `genre`
  MODIFY `id_genre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id_genre`) ON UPDATE CASCADE,
  ADD CONSTRAINT `events_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `account` (`id_account`) ON UPDATE CASCADE;

--
-- Constraints for table `pesan`
--
ALTER TABLE `pesan`
  ADD CONSTRAINT `pesan_ibfk_1` FOREIGN KEY (`id_account`) REFERENCES `account` (`id_account`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pesan_ibfk_2` FOREIGN KEY (`id_events`) REFERENCES `events` (`id_event`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
