<?php
SESSION_START();
include 'koneksi.php';
if(!isset($_SESSION['userid'])) {
  echo "<script>setTimeout(\"location.href='login.php';\",0);</script>";
}
 ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include 'core/header.php'; ?>
  </head>
  <body>
<!-- menu atas -->
<?php
include 'core/menu.php';
include 'home.php';
include 'core/menu_bawah.php';
?>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
