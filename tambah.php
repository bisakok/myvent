<?php
SESSION_START();
include 'koneksi.php';
if(!isset($_SESSION['userid'])) {
  echo "<script>setTimeout(\"location.href='login.php';\",0);</script>";
}
 ?>
 <!DOCTYPE html>
<html lang="en">
  <head>
    <?php include 'core/header.php'; ?>
    <style media="screen">

    .value-button {
      display: inline-block;
      border: 1px solid #ddd;
      margin: 0px;
      width: 40px;
      height: 40px;
      text-align: center;
      vertical-align: middle;
      /* padding: px 0; */
      background: #eee;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .value-button:hover {
      cursor: pointer;
    }

    form #decrease {
      margin-right: -4px;
      border-radius: 8px 0 0 8px;
    }

    form #increase {
      margin-left: -4px;
      border-radius: 0 8px 8px 0;
    }

    form #input-wrap {
      margin: 0px;
      padding: 0px;
    }

    input#number {
      text-align: center;
      border: none;
      border-top: 1px solid #ddd;
      border-bottom: 1px solid #ddd;
      margin: 0px;
      width: 40px;
      height: 40px;
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    </style>
  </head>
  <body>

      <?php  include 'core/menu.php';?>
        <center><img src="logo.png" class="img" style="width:30%;margin-top:5px;margin-bottom:10px">
          <h4><b>BUAT EVENT</b></h4>

          <form class="form" action="" method="post" enctype="multipart/form-data" style="margin-bottom:50px;">
            <!-- Select Basic -->
<div class="form-group">
  <div class="col-xs-12">
    <select id="selectbasic" name="genre" class="form-control">
      <?php
      $data_genre = mysqli_query($koneksi,"SELECT * from `genre`");
      $ada = mysqli_num_rows($data_genre);
       ?>
       <?php
       while ($a = mysqli_fetch_assoc($data_genre)):
        ?>
        <option value="<?php echo $a['id_genre'] ?>"><?php echo $a['name'] ?></option>
      <?php endwhile; ?>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <div class="col-xs-12">
  <input type="text" name="judul" placeholder="judul acara" class="form-control">
  </div>
</div>

<div class="form-group">
  <div class="col-xs-12">
  <input type="date" name="tanggal" placeholder="tanggal acara" class="form-control">
  </div>
</div>

<div class="form-group">
  <div class="col-xs-12">
  <input type="time" name="waktu" placeholder="waktu acara" class="form-control">
  </div>
</div>

<div class="form-group">
  <div class="col-xs-12">
  <input type="text" name="lokasi" placeholder="lokasi acara" class="form-control">
  </div>
</div>

<div class="form-group">
  <div class="col-xs-12">
    <textarea name="deskripsi" rows="8" cols="52%" placeholder="Deskripsi" class="form-control"></textarea>
  </div>
</div>


<div class="form-group">
  <div class="col-xs-12">
<input type="text" name="htm" placeholder="HTML" class="form-control">

  </div>
</div>

<div class="form-group">
  <div class="col-xs-12">
  <input type="number" name="jumlah" placeholder="Jumlah Tiket Tersedia" class="form-control">
  </div>
</div>

<div class="form-group">
  <div class="col-xs-12">
  <input type="file" name="pic" placeholder="POSTER" class="form-control">
</div>
</div>

<div class="form-group">
  <br>
    <input type="submit" class="btn form-control" name="simpan" value="SIMPAN"
    style="background-color:#0d2a4a;color:#FFF;padding:10px 10px 10px 10px;">
    <br><br>
</div>



          </form>
        </center>
<?php
  if(isset($_POST['simpan'])){
    $genre      = $_POST['genre'];
    $judul      = $_POST['judul'];
    $tanggal    = $_POST['tanggal'];
    $waktu      = $_POST['waktu'];
    $lokasi     = $_POST['lokasi'];
    $deskripsi  = $_POST['deskripsi'];
    $id_user    = $_SESSION['userid'];
    $harga      = $_POST['htm'];
    $jumlah     = $_POST['jumlah'];
    $pic_name   = $_FILES['pic']['name'];
    $pic_size   = $_FILES['pic']['size'];
    $pic_type   = $_FILES['pic']['type'];
    $pic_temp   = $_FILES['pic']['tmp_name'];

          if($pic_type == "image/png" || "image/jpg" || "image/jpeg"){
              echo "$pic_type<br>";
            if($pic_size < 2048000) {  //2mb
              echo "$pic_size<br>";
                $tujuan = "img/poster/$pic_name";
                $update = move_uploaded_file($pic_temp, $tujuan);
                if ($update) {
                  echo "$tujuan<br>";

$masuk = mysqli_query($koneksi,"INSERT INTO `events`(`id_event`, `id_genre`, `title`, `tanggal`, `waktu`,
  `pic`, `location`, `deskripsi`, `id_user`, `harga`, `jumlah_tiket`, `sisa_tiket`)
VALUES ('',$genre,'$judul','$tanggal',
  '$waktu','$tujuan','$lokasi','$deskripsi','$_SESSION[userid]',$harga,$jumlah,$jumlah)");
var_dump($masuk);
                  // echo "<script>setTimeout(\"location.href='index.php?p=timbaru';\",1000);</script>";
            }else {
              // echo "<script>setTimeout(\"location.href='index.php?p=timbaru';\",1000);</script>";
              }
                }
            echo "ukuran max 2mb";
                }
            echo "file harus jpg/ png";
        }
 ?>

        <?php  include 'core/menu_bawah.php';?>

    <script src="https://cdn.jsdelivr.net/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </body>
</html>
